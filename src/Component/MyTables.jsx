import React from "react";
import { Table, Button, Col } from "react-bootstrap";

export default function MyTables({ cars,onClean }) {
  //cheack ease item qty  > 0
  let i=0;
  let qtyCars = cars.filter((item) => {
      i+=item.total;
    return item.qty > 0;
  });


  return (
    <div style={{ marginTop: "50px" }}>
      <Col>
        <Button disabled={qtyCars.length===0} onClick={onClean} variant="danger">clear All</Button>{" "}
        <Button variant="warning">{qtyCars.length}</Button><br/><br />
      </Col>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Prices</th>
            <th>Qty</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {qtyCars.map((item, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{item.name}</td>
              <td>{item.price}M$</td>
              <td>{item.qty}</td>
              <td>{item.total}M$</td>
            </tr>
          ))}
          <tr>
            <td colSpan="3">#</td>
            <td>Payment</td>
            <td>{i}M$</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}
