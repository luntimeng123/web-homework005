import React, { Component } from "react";
import { Card, Button, Col} from "react-bootstrap";

export default class MyCards1 extends Component {
  render() {
    console.log("MyCars ", this.props.cars);

    return (
      <>
        {this.props.cars.map((item, index) => (
          <Col key={index}>
            <Card style={{ width: "18rem" }}>
              <Card.Img variant="top" src={item.pic} />

              <Card.Body>
                <Card.Title>{item.name}</Card.Title>
                <Card.Text>Price: {item.price} M$</Card.Text>

                <Button disabled={item.qty === 0?true:false} onClick={()=>this.props.onDecrease(index)} variant="danger">Delete</Button>{" "}
                <Button onClick={()=>this.props.onIncrease(index)}  variant="primary">Add</Button>{" "}
                <Button variant="success">{item.qty}</Button>
                <br /><br />

                <h3 style={{color:"gold"}}>
                    Totla {item.total} M$
                </h3>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </>
    );
  }
}
