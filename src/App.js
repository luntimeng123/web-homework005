import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import MyCards1 from './Component/MyCards1';
import { Container, Row } from 'react-bootstrap';
import MyTables from './Component/MyTables';


export default class App extends React.Component {
    
    //create state for array obejct
    constructor(props){
        super(props);

        this.state = {
            cars:[
                {
                    id:1,
                    name:"Audi",
                    pic:"/image/13652.jpg",
                    qty: 0,
                    price: 100,
                    total:0,
                },
                {
                    id:2,
                    name:"Lambo",
                    pic:"/image/car2.jpg",
                    qty: 0,
                    price: 200,
                    total:0,
                },
                {
                    id:3,
                    name:"Bugatti",
                    pic:"/image/car3.jpg",
                    qty: 0,
                    price: 300,
                    total:0,
                },
                {
                    id:4,
                    name:"Tesla",
                    pic:"/image/car4.jpg",
                    qty: 0,
                    price: 400,
                    total:0,
                },
            ]
        }
    }


    //incease
    onIncrease =(index)=>{
        console.log("index: ",index)
       let arrayCars = [...this.state.cars]
        arrayCars[index].qty++

        arrayCars[index].total = arrayCars[index].price * arrayCars[index].qty

        this.setState({
            cars:arrayCars
        })
    }

    //decrease
    onDecrease = (index)=>{
        let arrayCars = [...this.state.cars]
        arrayCars[index].qty--

        arrayCars[index].total = arrayCars[index].price * arrayCars[index].qty

        this.setState({
            cars:arrayCars
        })
    }

    onClean = () =>{
       let cleanArray = [...this.state.cars]

       cleanArray.map((item) =>{
            item.qty =0
            item.total =0
       })

       this.setState({
           cars:cleanArray
       })
    }


    render(){
        return (
            <Container>
                <h1>Cars Model</h1>
                <Row>
                    {/* assign array object cars to MyCrds */}
                    <MyCards1 cars={this.state.cars} onIncrease={this.onIncrease} onDecrease={this.onDecrease}/>
                </Row>
                <Row>
                    {/* table */}
                    <MyTables cars={this.state.cars} onClean={this.onClean}/>
                </Row>
            </Container>
        );
    }
}
